package day07

func countElements(arr []int) int {
	var tot int
	m := map[int]bool{}
	for _, v := range arr {
		m[v] = true
	}
	for _, v := range arr {
		if m[v+1] {
			tot++
		}
	}
	return tot
}
