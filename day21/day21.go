package day21

type BinaryMatrix interface {
	Get(int, int) int
	Dimensions() []int
}

func leftMostColumnWithOne(binaryMatrix BinaryMatrix) int {
	d, res := binaryMatrix.Dimensions(), -1
	rows, cols := d[0], d[1]
	r, c := 0, cols-1
	for r < rows && c >= 0 {
		if binaryMatrix.Get(r, c) == 1 {
			res = c
			c--
			continue
		}
		r++
	}
	return res
}
