# 30-day LeetCode Challenge

My solutions to the [30-day Challenge](https://leetcode.com/explore/challenge/card/30-day-leetcoding-challenge/) for April 2020.

## Challenges

1. [Day 1 - Single Number](./day01/day01.md)
2. [Day 2 - Happy Number](./day02/day02.md)
3. [Day 3 - Maximum subarray](./day03/day03.md)
4. [Day 4 - Move Zeroes](./day04/day04.md)
5. [Day 5 - Best Time to Buy and Sell Stock II](./day05/day05.md)
6. [Day 6 - Group Anagrams](./day06/day06.md)
7. [Day 7 - Counting Elements](./day07/day07.md)
8. [Day 8 - Middle of the Linked List](./day08/day08.md)
9. [Day 9 - Backspace String Compare](./day09/day09.md)
10. [Day 10 - Min Stack](./day10/day10.md)
11. [Day 11 - Diameter of Binary Tree](./day11/day11.md)
12. [Day 12 - Last Stone Weight](./day12/day12.md)
13. [Day 13 - Contiguous Array](./day13/day13.md)
14. [Day 14 - Perform String Shifts](./day14/day14.md)
15. [Day 15 - Product of Array Except Self](./day15/day15.md)
16. [Day 16 - Valid Parenthesis String](./day16/day16.md)
17. [Day 17 - Number of Islands](./day17/day17.md)
18. [Day 18 - Minimum Path Sum](./day18/day18.md)
19. [Day 19 - Search in a Rotated Array](./day19/day19.md)
20. [Day 20 - Construct Binary Search Tree from Preorder Traversal](./day20/day20.md)
21. [Day 21 - Leftmost Column with at Least a One](./day21/day21.md)
22. [Day 22 - Subarray Sum Equals K](./day22/day22.md)
23. [Day 23 - Bitwise AND of Numbers Range](./day23/day23.md)
24. [Day 24 - LRU Cache](./day24/day24.md)
25. [Day 25 - Jump Game](./day25/day25.md)
26. [Day 26 - Longest Common Subsequence](./day26/day26.md)
27. [Day 27 - Maximal Square](./day27/day27.md)
28. [Day 28 - First Unique Number](./day28/day28.md)
29. [Day 29 - Binary Tree Maximum Path Sum](./day29/day29.md)
30. [Day 30 - Check If a String Is a Valid Sequence from Root to Leaves Path in a Binary Tree](./day30/day30.md)

_And that's a wrap!_