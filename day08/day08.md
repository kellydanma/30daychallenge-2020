# Middle of the Linked List

Given a non-empty, singly linked list with head node `head`, return a middle node of linked list.

If there are two middle nodes, return the second middle node.

**Note**: The number of nodes in the given list will be between 1 and 100.

## Examples

### Example 1

```
Input:  [1,2,3,4,5]
Output: Node 3 from this list
```

### Example 2

```
Input:  [1,2,3,4,5,6]
Output: Node 4 from this list
```

## Solution

See the solution [here](./day08.go).


