package day08

type listNode struct {
	Val  int
	Next *listNode
}

func middleNode(head *listNode) *listNode {
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	return slow
}
