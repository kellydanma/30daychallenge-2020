package day28

type FirstUnique struct {
	Head   *DLLNode
	Tail   *DLLNode
	Unique map[int]*DLLNode
}

func Constructor(nums []int) FirstUnique {
	f := FirstUnique{
		Head:   nil,
		Tail:   nil,
		Unique: map[int]*DLLNode{},
	}
	for _, n := range nums {
		l, ok := f.Unique[n]
		if !ok {
			l = &DLLNode{Val: n}
			f.Unique[n] = l
			f.addToList(l)
		} else {
			f.removeFromList(l)
		}

	}
	return f
}

func (this *FirstUnique) ShowFirstUnique() int {
	if this.Tail == nil {
		return -1
	}
	return this.Tail.Val
}

func (this *FirstUnique) Add(value int) {
	l, ok := this.Unique[value]
	if !ok {
		l = &DLLNode{Val: value}
		this.Unique[value] = l
		this.addToList(l)
	} else {
		this.removeFromList(l)
	}
}

func (f *FirstUnique) addToList(l *DLLNode) {
	l.Next = nil
	if f.Head != nil {
		f.Head.Next = l
		l.Prev = f.Head
	}
	f.Head = l
	if f.Tail == nil {
		f.Tail = l
	}
}

func (f *FirstUnique) removeFromList(l *DLLNode) {
	if l.Removed {
		return // already removed
	}
	if l == f.Head {
		f.Head = l.Prev
	}
	if l == f.Tail {
		f.Tail = l.Next
	}
	if l.Next != nil {
		l.Next.Prev = l.Prev
	}
	if l.Prev != nil {
		l.Prev.Next = l.Next
	}
	l.Removed = true
}

// DLLNode is a doubly-linked list node.
type DLLNode struct {
	Val     int
	Removed bool
	Prev    *DLLNode
	Next    *DLLNode
}
