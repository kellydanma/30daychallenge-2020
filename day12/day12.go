package day12

func lastStoneWeight(stones []int) int {
	for len(stones) > 1 {
		x, xpos, y, ypos := stones[0], 0, stones[1], 1
		if x > y {
			x, xpos, y, ypos = y, ypos, x, xpos
		}
		for i := 2; i < len(stones); i++ {
			if s := stones[i]; s > y {
				x, xpos, y, ypos = y, ypos, s, i
				continue
			} else if s > x {
				x, xpos = s, i
			}
		}
		if x == y && xpos > ypos {
			xpos, ypos = ypos, xpos
		}
		if x == y && len(stones) == 2 {
			return 0
		}
		if x == y {
			a, b, c := stones[:xpos], stones[xpos+1:ypos], stones[ypos+1:]
			stones = append(append(a, b...), c...)
			continue
		}
		stones[ypos] = y - x
		a, b := stones[:xpos], stones[xpos+1:]
		stones = append(a, b...)
	}
	return stones[0]
}
