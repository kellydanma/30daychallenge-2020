package day15

func productExceptSelf(nums []int) []int {
	l, r := make([]int, len(nums)), make([]int, len(nums))
	for i := range nums {
		if i == 0 {
			l[i] = 1
			r[len(nums)-1] = 1
			continue
		}
		l[i] = l[i-1] * nums[i-1]
		r[len(nums)-1-i] = r[len(nums)-i] * nums[len(nums)-i]
	}
	for i := range nums {
		nums[i] = l[i] * r[i]
	}
	return nums
}
