package day16

func checkValidString(s string) bool {
	lo, hi := 0, 0 // smallest and largest possible number of open left brackets after processing the current character in the string
	for _, r := range s {
		switch r {
		case '(':
			lo++
			hi++
		case ')':
			lo--
			hi--
		default:
			lo--
			hi++
		}
		if hi < 0 {
			break
		}
		lo = max(lo, 0)
	}
	return lo == 0
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

// Greedy algorithm
// Try using a stack next time.
