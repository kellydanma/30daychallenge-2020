package day22

func subarraySum(nums []int, k int) int {
	count, sum, m := 0, 0, make(map[int]int)
	m[sum] = 1
	for _, n := range nums {
		sum += n
		if v, ok := m[sum-k]; ok {
			count += v
		}
		m[sum]++
	}
	return count
}
