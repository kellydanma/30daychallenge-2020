package day17

func numIslands(grid [][]byte) int {
	if len(grid) == 0 {
		return 0
	}
	var islands int
	for i := range grid {
		for j := range grid[i] {
			islands += dfs(i, j, grid)
		}
	}
	return islands
}

func dfs(i, j int, byte [][]byte) int {
	if i < 0 || j < 0 || i >= len(byte) || j >= len(byte[0]) {
		return 0
	}
	if byte[i][j] == '1' {
		byte[i][j] = '2'
		_ = dfs(i-1, j, byte)
		_ = dfs(i+1, j, byte)
		_ = dfs(i, j-1, byte)
		_ = dfs(i, j+1, byte)
		return 1
	}
	return 0
}
