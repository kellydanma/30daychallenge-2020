package day24

type LRUCache struct {
	Capacity int
	Head     *DLLNode
	Tail     *DLLNode
	Cache    map[int]*DLLNode
}

func Constructor(Capacity int) LRUCache {
	return LRUCache{
		Capacity: Capacity,
		Cache:    make(map[int]*DLLNode),
	}
}

func (c *LRUCache) Get(Key int) int {
	l, ok := c.Cache[Key]
	if !ok {
		return -1
	}
	c.removeFromChain(l)
	c.addToChain(l)
	return l.Val
}

func (c *LRUCache) Put(Key int, Value int) {
	l, ok := c.Cache[Key]
	if !ok {
		l = &DLLNode{Key: Key, Val: Value}
		c.Cache[Key] = l
	} else {
		l.Val = Value
		c.removeFromChain(l)
	}
	c.addToChain(l)
	if len(c.Cache) > c.Capacity {
		l := c.Tail
		c.removeFromChain(l)
		delete(c.Cache, l.Key)
	}
}

func (c *LRUCache) addToChain(l *DLLNode) {
	l.Next = nil
	if c.Head != nil {
		c.Head.Next = l
		l.Prev = c.Head
	}
	c.Head = l
	if c.Tail == nil {
		c.Tail = l
	}
}

func (c *LRUCache) removeFromChain(l *DLLNode) {
	if l == c.Head {
		c.Head = l.Prev
	}
	if l == c.Tail {
		c.Tail = l.Next
	}
	if l.Next != nil {
		l.Next.Prev = l.Prev
	}
	if l.Prev != nil {
		l.Prev.Next = l.Next
	}
}

// DLLNode is a doubly-linked list node.
type DLLNode struct {
	Key  int
	Val  int
	Prev *DLLNode
	Next *DLLNode
}
