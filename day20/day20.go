package day20

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func bstFromPreorder(preorder []int) *TreeNode {
	if len(preorder) == 0 {
		return nil
	}
	root := &TreeNode{
		Val:   preorder[0],
		Left:  bstFromPreorder(less(preorder[0], preorder)),
		Right: bstFromPreorder(greater(preorder[0], preorder)),
	}
	return root
}

func less(n int, arr []int) []int {
	res := []int{}
	for _, v := range arr {
		if v < n {
			res = append(res, v)
		}
	}
	return res
}

func greater(n int, arr []int) []int {
	res := []int{}
	for _, v := range arr {
		if v > n {
			res = append(res, v)
		}
	}
	return res
}
