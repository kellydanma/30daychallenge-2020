package day10

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func diameterOfBinaryTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	var diameter int
	helper(root, &diameter)
	return diameter
}

func helper(root *TreeNode, diameter *int) int {
	if root == nil {
		return 0
	}
	l, r := helper(root.Left, diameter), helper(root.Right, diameter)
	*diameter = max(*diameter, l+r)
	return max(l, r) + 1
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
