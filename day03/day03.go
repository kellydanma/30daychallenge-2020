package day03

func maxSubArray(nums []int) int {
	cur, max := nums[0], nums[0]
	nums = nums[1:]
	for _, v := range nums {
		if v > cur+v {
			cur = v
		} else {
			cur += v
		}
		if cur > max {
			max = cur
		}
	}
	return max
}

// Remember to store 2 values, cur AND max.
