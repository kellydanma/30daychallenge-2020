package day01

func singleNumber(nums []int) int {
	nr := nums[0]
	nums = nums[1:]
	for _, v := range nums {
		nr = nr ^ v // bitwise XOR
	}
	return nr
}

// The trick here is to use the bitwise XOR.
// If 2 numbers are the same, then XOR will equal 0.
// If not, they will be equal to the new number.
// After going through the list, all numbers will
// cancel each other out, except the one with no match.
