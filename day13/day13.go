package day13

func findMaxLength(nums []int) int {
	m := make(map[int]int) // key: count, value: index
	m[0] = -1
	var max, count int
	for i, n := range nums {
		if n == 0 {
			count += -1
		}
		if n == 1 {
			count += 1
		}
		if v, ok := m[count]; ok {
			max = getMax(max, i-v)
		} else {
			m[count] = i
		}
	}
	return max
}

func getMax(x, y int) int {
	if x > y {
		return x
	}
	return y
}
