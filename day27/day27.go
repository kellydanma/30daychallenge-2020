package day27

func maximalSquare(matrix [][]byte) int {
	rows, cols := len(matrix), 0
	if rows > 0 {
		cols = len(matrix[0])
	}
	dp := make([]int, cols+1)
	maxLen, prev := 0, 0

	for i := 1; i <= rows; i++ {
		for j := 1; j <= cols; j++ {
			tmp := dp[j]
			if matrix[i-1][j-1] == '1' {
				dp[j] = min(min(dp[j-1], prev), dp[j]) + 1
				maxLen = max(maxLen, dp[j])
			} else {
				dp[j] = 0
			}
			prev = tmp
		}
	}
	return maxLen * maxLen
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
