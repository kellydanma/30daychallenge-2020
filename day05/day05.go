package day05

func maxProfit(prices []int) int {
	if len(prices) == 0 {
		return 0
	}
	buy, sell, tot := prices[0], prices[0], 0
	prices = prices[1:]
	for _, p := range prices {
		if p >= sell {
			sell = p
			continue
		}
		if p < sell {
			tot += sell - buy
			buy, sell = p, p
		}
	}
	return tot + (sell - buy)
}
