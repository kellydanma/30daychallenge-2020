package day30

// TreeNode is a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isValidSequence(root *TreeNode, arr []int) bool {
	if root == nil {
		return false
	}
	if root.Val != arr[0] {
		return false
	}
	if len(arr) == 1 {
		return root.Left == nil && root.Right == nil
	}
	arr = arr[1:]
	return isValidSequence(root.Left, arr) || isValidSequence(root.Right, arr)
}
