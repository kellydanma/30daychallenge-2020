package day06

import (
	"sort"
	"strings"
)

func groupAnagrams(strs []string) [][]string {
	ret := [][]string{}
	tmp := map[string][]string{}
	for _, str := range strs {
		org := str
		ss := strings.Split(str, "")
		sort.Strings(ss)
		str = strings.Join(ss, "")
		tmp[str] = append(tmp[str], org)
	}
	for _, s := range tmp {
		ret = append(ret, s)
	}
	return ret
}

// Try to reimplement again.
