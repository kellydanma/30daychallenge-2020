package day14

func stringShift(s string, shift [][]int) string {
	var left, right int
	for _, sh := range shift {
		if sh[0] == 0 { // count left shifts
			left += sh[1]
		}
		if sh[0] == 1 { // count right shifts
			right += sh[1]
		}
	}
	if left > right { // overall left shift
		pivot := (left - right) % len(s)
		a, b := s[:pivot], s[pivot:]
		s = string(append([]rune(b), []rune(a)...))
	}
	if right > left { // overall right shift
		pivot := (right - left) % len(s)
		a, b := s[len(s)-pivot:], s[:len(s)-pivot]
		s = string(append([]rune(a), []rune(b)...))
	}
	return s
}
