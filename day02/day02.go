package day02

func isHappy(n int) bool {
	seen := map[int]bool{}
	for n > 1 {
		if seen[n] {
			return false
		}
		seen[n] = true
		n = pdiOf(n)
	}
	return n == 1
}

// perfect digital invariant function
// https://en.wikipedia.org/wiki/Perfect_digital_invariant
func pdiOf(n int) int {
	total := 0
	for n > 0 {
		rem := n % 10
		total += rem * rem
		n /= 10
	}
	return total
}
