package day29

// TreeNode is a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxPathSum(root *TreeNode) int {
	sum := root.Val
	helper(root, &sum)
	return sum
}

func helper(node *TreeNode, sum *int) int {
	if node == nil {
		return 0
	}
	l, r := helper(node.Left, sum), helper(node.Right, sum)
	*sum = max(*sum, node.Val+max(l, 0)+max(r, 0))
	return node.Val + max(0, max(r, l))
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
