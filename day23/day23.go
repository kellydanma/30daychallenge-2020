package day23

func rangeBitwiseAnd(m int, n int) int {
	ret := m
	for i := m + 1; i <= n; i++ {
		ret = ret & i
	}
	return ret
}
