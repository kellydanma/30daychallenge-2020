package day19

func search(nums []int, target int) int {
	left, right := 0, len(nums)-1
	for left < right {
		mid := (left + right) / 2
		if nums[mid] == target {
			return mid
		}
		if nums[left] <= nums[mid] {
			if target < nums[mid] && target >= nums[left] {
				right = mid - 1
				continue
			}
			left = mid + 1
			continue
		}
		if target > nums[mid] && target <= nums[right] {
			left = mid + 1
			continue
		}
		right = mid - 1
	}
	if right >= 0 && nums[left] == target {
		return left
	}
	return -1
}
